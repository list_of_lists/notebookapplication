﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    public class Note
    {
        public long phoneNumber; //obligatory
        public string firstName; //obligatory
        public string middleName;
        public string lastName; //obligatory
        public string country; //obligatory
        public string organization;
        public string position;
        public string otherNotes;
        public DateTime birthDate = DateTime.MinValue;

        public void CopyNote(Note note)
        {
            phoneNumber = note.phoneNumber;
            firstName = note.firstName;
            middleName = note.middleName;
            lastName = note.lastName;
            country = note.country;
            organization = note.organization;
            position = note.position;
            otherNotes = note.otherNotes;
            birthDate = note.birthDate;
        }
    }
}
