﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    public class DataParser
    {
        public bool isCorrectStr = false;

        public bool IsNullStr(string str, Note note)
        {
            if (str == "" || str == null)
            {
                Interface.Report(0, note);
                return true;
            }
            return false;
        }


        public bool ParseNamesJobsCountry(string str, Note note)
        {
            str = str.Trim();
            if (IsNullStr(str, note))
                return false;
            else
            {
                isCorrectStr = true;
                for (int i = 0; i < str.Length && isCorrectStr; i++)
                {
                    if (!char.IsLetter(str[i]))
                    {
                        isCorrectStr = false;
                        Interface.Report(1, note);
                    }
                }

                return isCorrectStr;
            }
        }

        public bool ParsePhoneNumber(string str, Note note)
        {
            str = str.Trim();
            if (IsNullStr(str, note))
                return false;
            else
            {
                isCorrectStr = true;
                if (str.Length < 2 || str.Length > 16)
                {
                    isCorrectStr = false;
                    Interface.Report(3, note);
                }
                for (int i = 0; i < str.Length && isCorrectStr; i++)
                {
                    if (!char.IsDigit(str[i]))
                    {
                        isCorrectStr = false;
                        Interface.Report(1, note);
                    }
                }

                return isCorrectStr;
            }
        }

        public bool ParseBirthDate(string str, Note note, out DateTime birthDate)
        {
            str = str.Trim();
            birthDate = DateTime.MinValue;

            if (IsNullStr(str, note))
                return false;
            else
            {
                isCorrectStr = true;
                string[] separated = str.Split('.');
                if (separated.Length > 3)
                {
                    isCorrectStr = false;
                    Interface.Report(1, note);
                }
                else
                {
                    for (int j = 0; j < separated.Length && isCorrectStr; j++)
                    {
                        for (int i = 0; i < separated[j].Length && isCorrectStr; i++)
                        {
                            if (separated[j].Length != 2)
                            {
                                isCorrectStr = false;
                                Interface.Report(1, note);
                            }
                            else if (!char.IsDigit(separated[j][i]))
                            {
                                isCorrectStr = false;
                                Interface.Report(1, note);
                            }
                        }
                    }
                    if (isCorrectStr)
                    {
                        try
                        {
                            birthDate = DateTime.Parse(str);
                            return true;
                        }
                        catch (Exception)
                        {
                            isCorrectStr = false;
                            Interface.Report(4, note);
                            return false;
                        }
                    }
                }
            }

            return isCorrectStr;
        }

        public bool ParseOtherNotes(string str, Note note)
        {
            str = str.Trim();
            if (IsNullStr(str, note))
                return false;
            else
            {
                isCorrectStr = true;
            }

            return isCorrectStr;
        }

        public bool ParseId(string str, Note note, out int id)
        {
            id = -1;

            str = str.Trim();
            if (IsNullStr(str, note))
                return false;
            else
            {
                isCorrectStr = true;
                for (int i = 0; i < str.Length && isCorrectStr; i++)
                {
                    if (!char.IsDigit(str[i]))
                    {
                        isCorrectStr = false;
                        Interface.Report(5, note);
                    }
                }
                if (isCorrectStr)
                {
                    try
                    {
                        id = int.Parse(str);
                    }
                    catch (Exception)
                    {
                        isCorrectStr = false;
                        Interface.Report(5, note);
                    }
                }

                return isCorrectStr;
            }
        }

    }
}
