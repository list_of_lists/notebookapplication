﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    class Notebook
    {
        public static Dictionary<int, Note> dictNotebook = new Dictionary<int, Note>();
        public static int id = -1;
        public static Option option;

        static void Main(string[] args)
        {
            do
            {
                Interface.menuOption = Interface.MenuOption.Main;
                option = (Option)Interface.EnterOption();
            } while (option != Option.Back);
        }

        public static void CreateNote()
        {
            Interface.menuOption = Interface.MenuOption.Create;
            Note note = new Note();
            DataParser parser = new DataParser();

            do
            {
                string str;
                option = (Option)Interface.EnterOption();

                if ((option >= Option.LastName && option <= Option.MiddleName) || option == Option.Country || option == Option.Organization || option == Option.Position)
                {
                    str = Console.ReadLine().Trim();
                    if (parser.ParseNamesJobsCountry(str, note))
                    {
                        switch (option)
                        {
                            case Option.LastName:
                                note.lastName = str;
                                break;
                            case Option.FirstName:
                                note.firstName = str;
                                break;
                            case Option.MiddleName:
                                note.middleName = str;
                                break;
                            case Option.Country:
                                note.country = str;
                                break;
                            case Option.Organization:
                                note.organization = str;
                                break;
                            case Option.Position:
                                note.position = str;
                                break;
                        }
                    }
                }
                else if (option == Option.PhoneNumber)
                {
                    str = Console.ReadLine().Trim();
                    if (parser.ParsePhoneNumber(str, note))
                        note.phoneNumber = long.Parse(str);
                }
                else if (option == Option.BirthDate)
                {
                    str = Console.ReadLine().Trim();
                    parser.ParseBirthDate(str, note, out note.birthDate);
                }
                else if (option == Option.OtherNotes)
                {
                    str = Console.ReadLine().Trim();
                    if (parser.ParseOtherNotes(str, note))
                        note.otherNotes = str;
                }
                else if (option == Option.Submit) //create Note
                {
                    if (note.lastName == null || note.firstName == null || note.phoneNumber == 0 || note.country == null)
                    {
                        Interface.Report(2, note);
                    }
                    else
                    {
                        Interface.Report(-1, note);
                        id++;
                        dictNotebook.Add(id, note);
                        note = new Note();
                    }
                }

            } while (option != Option.Back); //until we press Esc

        }

        public static void EditNote(int id)
        {
            Interface.menuOption = Interface.MenuOption.Edit;
            Note note = null;

            if (!dictNotebook.ContainsKey(id))
                Interface.Report(5, note);
            else
            {
                note = new Note();
                note.CopyNote(dictNotebook[id]);
                DataParser parser = new DataParser();

                do
                {
                    string str;
                    option = (Option)Interface.EnterOption();

                    if ((option >= Option.LastName && option <= Option.MiddleName) || option == Option.Country || option == Option.Organization || option == Option.Position)
                    {
                        str = Console.ReadLine().Trim();
                        if (parser.ParseNamesJobsCountry(str, note))
                        {
                            switch (option)
                            {
                                case Option.LastName:
                                    note.lastName = str;
                                    break;
                                case Option.FirstName:
                                    note.firstName = str;
                                    break;
                                case Option.MiddleName:
                                    note.middleName = str;
                                    break;
                                case Option.Country:
                                    note.country = str;
                                    break;
                                case Option.Organization:
                                    note.organization = str;
                                    break;
                                case Option.Position:
                                    note.position = str;
                                    break;
                            }
                        }
                        else
                        {
                            switch (option)
                            {
                                case Option.LastName:
                                    note.lastName = null;
                                    break;
                                case Option.FirstName:
                                    note.firstName = null;
                                    break;
                                case Option.MiddleName:
                                    note.middleName = null;
                                    break;
                                case Option.Country:
                                    note.country = null;
                                    break;
                                case Option.Organization:
                                    note.organization = null;
                                    break;
                                case Option.Position:
                                    note.position = null;
                                    break;
                            }
                        }
                    }
                    else if (option == Option.PhoneNumber)
                    {
                        str = Console.ReadLine().Trim();
                        if (parser.ParsePhoneNumber(str, note))
                            note.phoneNumber = long.Parse(str);
                        else
                            note.phoneNumber = 0;
                    }
                    else if (option == Option.BirthDate)
                    {
                        str = Console.ReadLine().Trim();
                        parser.ParseBirthDate(str, note, out note.birthDate);
                    }
                    else if (option == Option.OtherNotes)
                    {
                        str = Console.ReadLine().Trim();
                        if (parser.ParseOtherNotes(str, note))
                            note.otherNotes = str;
                        else
                            note.otherNotes = null;
                    }
                    else if (option == Option.Submit) //apply changes to Note
                    {
                        if (note.lastName == null || note.firstName == null || note.phoneNumber == 0 || note.country == null)
                        {
                            Interface.Report(2, note);
                        }
                        else
                        {
                            dictNotebook[id] = note;
                            Console.WriteLine(dictNotebook[id].organization);
                            Interface.Report(-2, note);
                        }
                    }

                } while (option != Option.Back); //until we press Esc

            }
        }

        public static void DeleteNote(int id)
        {
            Interface.menuOption = Interface.MenuOption.Delete;
            Note note = null;

            if (!dictNotebook.ContainsKey(id))
                Interface.Report(5, note);
            else
            {
                Dictionary<int, Note> tmp = new Dictionary<int, Note>();
                Notebook.id = -1;

                foreach (var i in dictNotebook)
                {
                    if (i.Key != id)
                    {
                        Notebook.id++;
                        tmp.Add(Notebook.id, i.Value);
                    }
                }

                dictNotebook = tmp;
                Interface.Report(-3, note);
            }
        }

        public static void ShowNote(int id)
        {
            const string idMsg = "id: ";
            const string lastNameMsg = "Фамилия: ";
            const string firstNameMsg = "Имя: ";
            const string middleNameMsg = "Отчество: ";
            const string phoneNumberMsg = "Номер телефона: ";
            const string countryMsg = "Страна: ";
            const string birthDateMsg = "Дата рождения: ";
            const string organizationMsg = "Организация: ";
            const string positionMsg = "Должность: ";
            const string otherNotesMsg = "Прочие заметки: ";

            Interface.menuOption = Interface.MenuOption.Show;
            Note note = null;

            if (!dictNotebook.ContainsKey(id))
                Interface.Report(5, note);
            else
            {
                note = dictNotebook[id];

                Console.WriteLine(idMsg + id);
                if (note.lastName != null && note.lastName != "")
                    Console.WriteLine(lastNameMsg + note.lastName);
                if (note.firstName != null && note.firstName != "")
                    Console.WriteLine(firstNameMsg + note.firstName);
                if (note.middleName != null && note.middleName != "")
                    Console.WriteLine(middleNameMsg + note.middleName);
                if (note.phoneNumber != 0)
                    Console.WriteLine(phoneNumberMsg + note.phoneNumber);
                if (note.country != null && note.country != "")
                    Console.WriteLine(countryMsg + note.country);
                if (note.birthDate != DateTime.MinValue)
                    Console.WriteLine(birthDateMsg + note.birthDate.ToLongDateString());
                if (note.organization != null && note.organization != "")
                    Console.WriteLine(organizationMsg + note.organization);
                if (note.position != null && note.position != "")
                    Console.WriteLine(positionMsg + note.position);
                if (note.otherNotes != null && note.otherNotes != "")
                    Console.WriteLine(otherNotesMsg + note.otherNotes);

                Interface.Report(-4, note);
            }
        }

        public static void ShowAllNotes()
        {
            const string idMsg = "id: ";
            const string lastNameMsg = "Фамилия: ";
            const string firstNameMsg = "Имя: ";
            const string phoneNumberMsg = "Номер телефона: ";

            Interface.menuOption = Interface.MenuOption.ShowAll;
            Note note = null;

            foreach (var i in dictNotebook)
            {
                Console.WriteLine(idMsg + i.Key);
                Console.WriteLine(lastNameMsg + i.Value.lastName);
                Console.WriteLine(firstNameMsg + i.Value.firstName);
                Console.WriteLine(phoneNumberMsg + i.Value.phoneNumber);
                Console.WriteLine();
            }

            if (id == -1)
                Interface.Report(6, note);
            else
                Interface.Report(-4, note);
        }

        public enum Option
        {
            Back = -1,
            Submit = 0,
            LastName = 1,
            FirstName,
            MiddleName,
            PhoneNumber,
            Country,
            BirthDate,
            Organization,
            Position,
            OtherNotes
        }

    }
}
