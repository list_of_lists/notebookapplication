﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    public class Interface
    {
        public static MenuOption menuOption = MenuOption.Main;

        public static int Report(int code, Note note)
        {
            const string emptyLastName = "Вы не заполнили обязательное поле - фамилия!";
            const string emptyFirstName = "Вы не заполнили обязательное поле - имя!";
            const string emptyPhoneNumber = "Вы не заполнили обязательное поле - номер телефона!";
            const string emptyCountry = "Вы не заполнили обязательное поле - страна!";
            const string nullData = "Вы не ввели данные!";
            const string incorrectData = "Вы ввели некорректные данные!";
            const string incorrectLength = "Вы ввели данные некорректной длины!";
            const string incorrectBirthDate = "Такая дата рождения не может быть корректной!";
            const string invalidId = "Запись с таким id не существует!";
            const string noNotes = "В записной книжке нет записей!";
            const string toMainMenu = "Нажмите клавишу 'Enter' или 'Esc' для возврата в главное меню.";
            const string toPrevMenu = "Нажмите клавишу 'Enter' или 'Esc' для возврата в предыдущее меню.";
            const string successCreate = "Запись успешно создана!";
            const string successEdit = "Запись успешно отредактирована!";
            const string successDelete = "Запись успешно удалена!";

            bool isCorrectNote = true;

            switch (code)
            {
                case -4:
                    Console.WriteLine();
                    Console.WriteLine(toMainMenu);
                    break;
                case -3:
                    Console.Clear();
                    Console.WriteLine(successDelete);
                    Console.WriteLine();
                    Console.WriteLine(toMainMenu);
                    break;
                case -2:
                    Console.Clear();
                    Console.WriteLine(successEdit);
                    Console.WriteLine();
                    Console.WriteLine(toPrevMenu);
                    break;
                case -1:
                    Console.Clear();
                    Console.WriteLine(successCreate);
                    Console.WriteLine();
                    Console.WriteLine(toPrevMenu);
                    break;
                case 0:
                    Console.Clear();
                    Console.WriteLine(nullData);
                    Console.WriteLine();
                    Console.WriteLine(toPrevMenu);
                    break;
                case 1:
                    Console.Clear();
                    Console.WriteLine(incorrectData);
                    Console.WriteLine();
                    Console.WriteLine(toPrevMenu);
                    break;
                case 2:
                    Console.Clear();
                    isCorrectNote = false;
                    break;
                case 3:
                    Console.Clear();
                    Console.WriteLine(incorrectLength);
                    Console.WriteLine();
                    Console.WriteLine(toPrevMenu);
                    break;
                case 4:
                    Console.Clear();
                    Console.WriteLine(incorrectBirthDate);
                    Console.WriteLine();
                    Console.WriteLine(toPrevMenu);
                    break;
                case 5:
                    Console.Clear();
                    Console.WriteLine(invalidId);
                    Console.WriteLine();
                    Console.WriteLine(toMainMenu);
                    break;
                case 6:
                    Console.Clear();
                    Console.WriteLine(noNotes);
                    Console.WriteLine();
                    Console.WriteLine(toMainMenu);
                    break;
            }

            if (!isCorrectNote)
            {
                if (note.lastName == null)
                    Console.WriteLine(emptyLastName);
                if (note.firstName == null)
                    Console.WriteLine(emptyFirstName);
                if (note.phoneNumber == 0)
                    Console.WriteLine(emptyPhoneNumber);
                if (note.country == null)
                    Console.WriteLine(emptyCountry);
                Console.WriteLine(toPrevMenu);
            }

            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey(true);
            } while (key.Key != ConsoleKey.Escape && key.Key != ConsoleKey.Enter);

            if (key.Key == ConsoleKey.Escape)
                return -1;
            return 0;
        }

        public static void PrintCreateOptions()
        {
            const string messageChooseOption = "Нажмите клавишу для выбора нужной опции ввода соответствующих данных:";
            const string messageBack = "'Esc' - возврат в предыдущее меню.";
            const string messageAdd = "'Enter' - добавление записи.";
            const string option1Message = "1. Ввести фамилию.";
            const string option2Message = "2. Ввести имя.";
            const string option3Message = "3. Ввести отчество (опционально).";
            const string option4Message = "4. Ввести номер телефона.";
            const string option5Message = "5. Ввести страну.";
            const string option6Message = "6. Ввести дату рождения (опционально).";
            const string option7Message = "7. Ввести организацию (опционально).";
            const string option8Message = "8. Ввести должность (опционально).";
            const string option9Message = "9. Ввести прочие заметки (опционально).";

            Console.WriteLine(messageChooseOption);
            Console.WriteLine(messageBack);
            Console.WriteLine(messageAdd);
            Console.WriteLine(option1Message);
            Console.WriteLine(option2Message);
            Console.WriteLine(option3Message);
            Console.WriteLine(option4Message);
            Console.WriteLine(option5Message);
            Console.WriteLine(option6Message);
            Console.WriteLine(option7Message);
            Console.WriteLine(option8Message);
            Console.WriteLine(option9Message);
        }

        public static void PrintEditOptions()
        {
            const string messageChooseOption = "Нажмите клавишу для выбора нужной опции редактирования соответствующих данных:";
            const string messageBack = "'Esc' - возврат в предыдущее меню.";
            const string messageAdd = "'Enter' - сохранить внесенные изменения в запись.";
            const string option1Message = "1. Редактировать фамилию.";
            const string option2Message = "2. Редактировать имя.";
            const string option3Message = "3. Редактировать отчество.";
            const string option4Message = "4. Редактировать номер телефона.";
            const string option5Message = "5. Редактировать страну.";
            const string option6Message = "6. Редактировать дату рождения.";
            const string option7Message = "7. Редактировать организацию.";
            const string option8Message = "8. Редактировать должность.";
            const string option9Message = "9. Редактировать прочие заметки.";

            Console.WriteLine(messageChooseOption);
            Console.WriteLine(messageBack);
            Console.WriteLine(messageAdd);
            Console.WriteLine(option1Message);
            Console.WriteLine(option2Message);
            Console.WriteLine(option3Message);
            Console.WriteLine(option4Message);
            Console.WriteLine(option5Message);
            Console.WriteLine(option6Message);
            Console.WriteLine(option7Message);
            Console.WriteLine(option8Message);
            Console.WriteLine(option9Message);
        }

        public static void PrintMenuOptions()
        {
            const string messageChooseOption = "Нажмите клавишу для выбора нужной опции работы с записной книжкой:";
            const string messageBack = "'Esc' - выход из программы.";
            const string option1Message = "1. Создать новую запись.";
            const string option2Message = "2. Редактировать запись.";
            const string option3Message = "3. Удалить запись.";
            const string option4Message = "4. Просмотреть запись.";
            const string option5Message = "5. Просмотреть все записи.";

            Console.WriteLine(messageChooseOption);
            Console.WriteLine(messageBack);
            Console.WriteLine(option1Message);
            Console.WriteLine(option2Message);
            Console.WriteLine(option3Message);
            Console.WriteLine(option4Message);
            Console.WriteLine(option5Message);
        }

        public static int EnterOption()
        {
            const string enterIdToEdit = "Введите id записи для редактирования (индексация записей начинается с 0):";
            const string enterIdToDelete = "Введите id записи для удаления (индексация записей начинается с 0):";
            const string enterIdToShow = "Введите id записи, которую хотите просмотреть (индексация записей начинается с 0):";
            const string enterLastName = "Введите фамилию:";
            const string enterFirstName = "Введите имя:";
            const string enterMiddleName = "Введите отчество:";
            const string enterPhoneNumber = "Введите номер телефона (2-16 символов, только цифры):";
            const string enterCountry = "Введите страну:";
            const string enterBirthDate = "Введите дату рождения (в формате дд.мм.гг):";
            const string enterOrganization = "Введите организацию:";
            const string enterPosition = "Введите должность:";
            const string enterOtherNotes = "Введите прочие заметки:";

            int option = 0;
            bool isOption;

            do
            {
                isOption = true;
                Console.Clear();
                switch (menuOption)
                {
                    case MenuOption.Main:
                        PrintMenuOptions();
                        break;
                    case MenuOption.Create:
                        PrintCreateOptions();
                        break;
                    case MenuOption.Edit:
                        PrintEditOptions();
                        break;
                }
                var key = Console.ReadKey(true);
                Console.WriteLine();
                Console.Clear();

                switch (key.Key)
                {
                    case ConsoleKey.D1:
                        if (menuOption == MenuOption.Main)
                            Notebook.CreateNote();
                        else if (menuOption == MenuOption.Create || menuOption == MenuOption.Edit)
                            Console.WriteLine(enterLastName);
                        option = 1;
                        break;
                    case ConsoleKey.D2:
                        if (menuOption == MenuOption.Main)
                        {
                            Console.WriteLine(enterIdToEdit);
                            DataParser parser = new DataParser();
                            Note note = null;

                            if (parser.ParseId(Console.ReadLine(), note, out int id))
                                Notebook.EditNote(id);
                        }
                        else if (menuOption == MenuOption.Create || menuOption == MenuOption.Edit)
                            Console.WriteLine(enterFirstName);
                        option = 2;
                        break;
                    case ConsoleKey.D3:
                        if (menuOption == MenuOption.Main)
                        {
                            Console.WriteLine(enterIdToDelete);
                            DataParser parser = new DataParser();
                            Note note = null;

                            if (parser.ParseId(Console.ReadLine(), note, out int id))
                                Notebook.DeleteNote(id);
                        }
                        else if (menuOption == MenuOption.Create || menuOption == MenuOption.Edit)
                            Console.WriteLine(enterMiddleName);
                        option = 3;
                        break;
                    case ConsoleKey.D4:
                        if (menuOption == MenuOption.Main)
                        {
                            Console.WriteLine(enterIdToShow);
                            DataParser parser = new DataParser();
                            Note note = null;

                            if (parser.ParseId(Console.ReadLine(), note, out int id))
                            {
                                Console.Clear();
                                Notebook.ShowNote(id);
                            }
                        }
                        else if (menuOption == MenuOption.Create || menuOption == MenuOption.Edit)
                            Console.WriteLine(enterPhoneNumber);
                        option = 4;
                        break;
                    case ConsoleKey.D5:
                        if (menuOption == MenuOption.Main)
                        {
                            Notebook.ShowAllNotes();
                        }
                        else if (menuOption == MenuOption.Create || menuOption == MenuOption.Edit)
                            Console.WriteLine(enterCountry);
                        option = 5;
                        break;
                    case ConsoleKey.D6:
                        if (menuOption != MenuOption.Main)
                            Console.WriteLine(enterBirthDate);
                        option = 6;
                        break;
                    case ConsoleKey.D7:
                        if (menuOption != MenuOption.Main)
                            Console.WriteLine(enterOrganization);
                        option = 7;
                        break;
                    case ConsoleKey.D8:
                        if (menuOption != MenuOption.Main)
                            Console.WriteLine(enterPosition);
                        option = 8;
                        break;
                    case ConsoleKey.D9:
                        if (menuOption != MenuOption.Main)
                            Console.WriteLine(enterOtherNotes);
                        option = 9;
                        break;
                    case ConsoleKey.Escape:
                        option = -1;
                        break;
                    case ConsoleKey.Enter:
                        if (menuOption != MenuOption.Main)
                            option = 0;
                        else
                            isOption = false;
                        break;
                    default:
                        isOption = false;
                        break;
                }
            } while (!isOption);

            return option;
        }

        public enum MenuOption
        {
            Main,
            Create,
            Edit,
            Delete,
            Show,
            ShowAll
        }

    }
}
